package fr.hagenbourger.mpg

import java.io._

import net.liftweb.json
import net.liftweb.json._

object MPG extends App {

  private def getIds: String = sys.env("LEAGUE_IDS")

  private def getJsonContent(id: String, page: String): String = {
    val source = io.Source.fromURL(s"https://api.monpetitgazon.com/league/$id/$page")
    val response = source.mkString
    source.close()
    response
  }

  private def getLeagueName(jsonContent: String): String = {
    implicit val formats: DefaultFormats = DefaultFormats
    val data: JValue = JsonParser.parse(jsonContent)
    (data \ "leagueName").extract[String]
  }

  private def getHtmlRanking(jsonContent: String): String = {
    implicit val formats: DefaultFormats = DefaultFormats
    val data: JValue = JsonParser.parse(jsonContent)
    val teams: json.JValue = data \ "teams"
    val rankings: (Int, List[Ranking]) = (data \ "ranking").children
      .foldLeft((0, List.empty[Ranking])) { (acc, item) =>
        val ranking: Ranking = item.extract[Ranking]
        (acc._1 + ranking.goalconceded, acc._2 ++ List(ranking))
      }
    s"""<table class="table table-striped">
      <thead class="thead-light">
      <tr>
      <th scope="col">Equipes</th>
      <th scope="col">#</th>
      <th scope="col">J</th>
      <th scope="col">V</th>
      <th scope="col">N</th>
      <th scope="col">D</th>
      <th scope="col">BP</th>
      <th scope="col">BC</th>
      <th scope="col">PTS</th>
      <th scope="col">Buts pris si &eacute;quitable</th>
      <th scope="col">% Chatte*</th>
      </tr>
      </thead>
      <tbody>
      ${rankings._2
      .map(_.toHtmlTable(teams, rankings._1, rankings._2.size))
      .mkString("")}
      </tbody>
      </table>"""
  }

  private def generateLeague(leagueName: String, htmlRanking: String): String = {
    io.Source
      .fromResource("league.html")
      .mkString
      .replace("${{title}}", s"MPG - ${leagueName}")
      .replace("${{rankings}}", htmlRanking)
  }

  private def generate(leagues: String): Unit = {
    val template: String = io.Source
      .fromResource("template.html")
      .mkString
      .replace("${{leagues}}", leagues)

    val pw = new PrintWriter(new File("target/index.html"))
    pw.write(template)
    pw.close()
  }

  val leagues: String = getIds
    .split(";")
    .map(id =>
      generateLeague(
        getLeagueName(getJsonContent(id, "status")),
        getHtmlRanking(getJsonContent(id, "ranking"))
      )
    )
    .mkString

  generate(leagues)

}
